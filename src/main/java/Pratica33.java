import utfpr.ct.dainf.if62c.exemplos.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @modified Lucio Agostinho Rocha <luciorocha at utfpr dot edu dot br>
 * @since 27/09/2016
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        
        //Teste de soma com mesma cardinalidade
        double [][]tabela1 = {{1,3},{5,7}};
        Matriz m1 = new Matriz(2,2);                        
        m1.setMatriz(tabela1);
        System.out.println("A:\n"+m1.toString());
        
        double [][]tabela2 = {{2,1},{3,2}};
        Matriz m2 = new Matriz(2,2);                        
        m2.setMatriz(tabela2);
        System.out.println("B:\n"+m2.toString());
                
        System.out.println("A+B:\n"+ m1.soma(m2).toString());        

        System.out.println("---");

        //Teste de Produto, mas com cardinalidade diferente
        double [][]tabela3 = {{1,3},{5,7},{4,2}};//3x2
        Matriz m3 = new Matriz(3,2);                        
        m3.setMatriz(tabela3);
        System.out.println("A:\n"+m3.toString());
        
        double [][]tabela4 = {{2,1,3},{3,2,4}};//2x3
        Matriz m4 = new Matriz(2,3);                        
        m4.setMatriz(tabela4);
        System.out.println("B:\n"+m4.toString());
                        
        System.out.println("A*B:\n"+ m3.prod(m4).toString());
    }
}
