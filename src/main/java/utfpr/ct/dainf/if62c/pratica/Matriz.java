package utfpr.ct.dainf.if62c.pratica;

public class Matriz {
    
    // a matriz representada por esta classe
    private final double[][] mat;
    
    /**
     * Construtor que aloca a matriz.
     * @param m O número de linhas da matriz.
     * @param n O número de colunas da matriz.
     */
    public Matriz(int m, int n) {
        mat = new double[m][n];
    }
    
    /**
     * Retorna a matriz representada por esta classe.
     * @return A matriz representada por esta classe
     */
    public double[][] getMatriz() {
        return mat;
    }
    
    /**
     * Atribui valores ah matriz representada por esta classe.
     * @return void
     */
    public void setMatriz(double[][]valores) {
        for (int i = 0; i < valores.length; i++) {//Linhas
            for (int j = 0; j < valores[i].length; j++) {//Colunas
                mat[i][j] = valores[i][j]; 
            }
        }
    }
    
    /**
     * Retorna a matriz transposta.
     * @return A matriz transposta.
     */
    public Matriz getTransposta() {
        Matriz t = new Matriz(mat[0].length, mat.length);
        for (int i = 0; i < mat.length; i++) {//Linhas
            for (int j = 0; j < mat[i].length; j++) {//Colunas
                t.mat[j][i] = mat[i][j];
            }
        }
        return t;
    }
    
    /**
     * Retorna a soma desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser somada
     * @return A soma das matrizes
     */
    public Matriz soma(Matriz m) {
        //throw new UnsupportedOperationException("Soma de matrizes não implementada.");        
        
        //System.out.println("Linhas:"+mat.length+" Colunas:"+mat[0].length);
        //System.out.println("Linhas:"+m.getMatriz().length+" Colunas:"+m.getMatriz()[0].length);        
        //System.exit(0);
        
        Matriz soma = new Matriz(mat.length, mat[0].length);        
        for (int i = 0; i < mat.length; i++) {//Linhas
            for (int j = 0; j < mat[0].length; j++) {//Colunas
                //System.out.print("\ni:"+i+" j:"+j+" m.mat[i][j]:"+m.mat[i][j]);
                //System.out.print(" mat[i][j]:"+mat[i][j]);
                soma.mat[i][j] = m.mat[i][j] + mat[i][j];
            }
        }
        return soma;
        
    }

    /**
     * Retorna o produto desta matriz com a matriz recebida como argumento.
     * @param m A matriz a ser multiplicada
     * @return O produto das matrizes
     */
    public Matriz prod(Matriz m) {
        //throw new UnsupportedOperationException("Produto de matrizes não implementado.");
        //Linha, coluna
        Matriz prod = new Matriz(mat.length, m.getMatriz()[0].length);
        for (int i = 0; i < mat.length; i++) { //linhas
            for (int j = 0; j < m.getMatriz()[0].length; j++) {//colunas
                for (int k=0; k < mat[0].length; k++ ){//colunas
                    prod.mat[i][j] += mat[i][k] * m.mat[k][j];                    
                }
            }
        }
        return prod;
        
    }

    /**
     * Retorna uma representação textual da matriz.
     * Este método não deve ser usado com matrizes muito grandes
     * pois não gerencia adequadamente o tamanho do string e
     * poderia provocar um uso excessivo de recursos.
     * @return Uma representação textual da matriz.
     */
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] linha: mat) {
            s.append("[ ");
            for (double x: linha) {
                s.append(x).append(" ");
            }
            s.append("]\n");
        }
        return s.toString();
    }
    
}
